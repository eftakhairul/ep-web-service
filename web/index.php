<?php
require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app['swiftmailer.options'] = array(
    'host'       => 'smtp.gmail.com',
    'port'       => 465,
    'username'   => 'develop@epanneur.ca',
    'password'   => 'rain2015',
    'encryption' => 'ssl',
    'auth_mode'  => 'login'
);

$app->post('/feedback', function () use ($app) {
    $request = $app['request'];

    $message = \Swift_Message::newInstance()
        ->setSubject('New Postal code')
        ->setFrom(array('develop@epanneur.ca'))
        ->setTo(array('geraldine@epanneur.ca'))
        ->setBody("Postal code: " . $request->get('postalform_input '));

    $app['mailer']->send($message);
    return new Response('Thank you for your feedback!', 201);
});

$app->run();